# Sesión 01
-------------------
#### Software utilizado
- [REST Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)
- [Node.js](https://nodejs.org/en/about/) - Versión LTS
- [Git](https://git-scm.com/)
   - [Git book](https://git-scm.com/book/es/v2)
#
#### Comandos básicos *NIX

| Comandos | Descripción | Ejemplo |
| ------- | ----------- | ------- |
| pwd | Indica en que directorio estamos parados | $ pwd|
| ls | Lista el contenido de un directorio | $ ls 2021-js-bootcamp |
| ls -l | Lista el contenido de un directorio especificando permisos, propietario, fecha, hora, etc | $ ls -l 2021-js-bootcamp |
| ls -la | Lista el contenido de un directorio especificando permisos, propietario, fecha, hora, ademas incluye los archivos ocultos | $ ls 2021-js-bootcamp |
| cd | Cambio de directorio | $ cd 2021-js-bootcamp |
| cd . . | Ir un directorio hacia arriba | $ cd . . |
| mkdir | Crea un directorio | $ mkdir 2021-js-bootcamp |
| touch | Crea un nuevo archivo | $ touch index.js |
| cp | Copiar un archivo a un directorio de destino | $ cp index.js src/ |
| cp -r | Copiar un directorio a un directorio de destino | $ cp -r js/ src/ |
| mv | Mover un directorio o archivo a directorio de destino | $ mv index.js src/ |
| vim / nano | Utilice cualquiera de estos para editar un archivo | $ vim index,js  |
| cat | Lista el contenido de un archivo | $ cat index.js |
| rm | Elimina un archivo | $ rm index,js |
| rm -r | Elimina un directorio | $ rm -r index,js |

#### Hello World con node.js
```
node src/index.js
```

#### First node project
Para inicializar un proyecto node ejecutamos en la terminal el siguiente comando:
```
$ npm init -y
```
Se creará un archivo `package.json` con valores por defecto, el cual podemos modificar si así lo deseamos más adelante. Si queremos ingresar los valores desde la terminal solo omitiremos `-y`

Para iniciar, ejecutar el siguiente comando en la terminal
```
$ npm start
```
`NOTA:` Hay que estar en la raíz del proyecto, es decir, donde se ve el archivo `package.json` y la carpeta `src`

Para poder probar el proyecto, en una terminal aparte, existen 3 maneras:

1. La petición más simple que se puede hacer
    ```
    curl http://127.0.0.1:3000/
    ```
2. Llamada verbosa para conocer lo que se envía y lo que se recibe
     ```
    curl -v http://127.0.0.1:3000/
    ```
3. Llamada verbosa con método, en este caso OPTIONS
     ```
    curl -v -X OPTIONS http://127.0.0.1:3000/
    ```

#### Algunos sitios de interes

  - [The best of JavaScript](https://bestofjs.org/)
  - [Class HTTP Server](https://nodejs.org/dist/latest-v14.x/docs/api/http.html#http_class_http_server)
  - [Server Listen](https://nodejs.org/dist/latest-v14.x/docs/api/net.html#net_server_listen)
  - [MIME Types](https://developer.mozilla.org/es/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types)
  - [HTTP Status](https://developer.mozilla.org/es/docs/Web/HTTP/Status)
  - [HTTP Headers](https://developer.mozilla.org/es/docs/Web/HTTP/Headers)

