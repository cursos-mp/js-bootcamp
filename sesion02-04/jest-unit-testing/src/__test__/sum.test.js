import sum from '../sum';

test('adds 1 + 2 to equal 3', () => {
  expect(sum(1, 2)).toBe(3);
});

test('esto es una prueba unitaria', () => {
  expect(sum(3, 6)).not.toBe(3)
  expect(sum(3, 6)).toBe(9)
  expect(sum('a', 'b')).toBe('ab')
  expect(sum('a', 'b', 'c')).not.toBe('abc')
  expect(sum('a', 'b', 'c')).toBe('ab')
})