export const templateString = (person) => `Bienvenido ${person.name} ${person.lastname}, entraste como ${person.role}!`

export function getProductHTML(product) {
  let html = `
    <div class="product">
      <div class="product-image">
        <img alt="${product.name}" src="${product.image_url}">
      </div>
      <div class="product-desc">${product.desc}</div>
    </div>`
  return html;
}

export const strStartsWith = (str, strToFind) => str.startsWith(strToFind)

export const strEndsWith = (str, strToFind) => str.endsWith(strToFind)

export const strIncludes = (str, strToFind) => str.includes(strToFind)

export const strRepeat = (str, times) => str.repeat(times)

export const strPadStart = (str, length, chars) => str.padStart(length, chars)

export const strPadEnd = (str, length, chars) => str.padEnd(length, chars)