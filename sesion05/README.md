# Sesión 05
-------------------
#### Intsalar dependencia browser-sync
Para instalar browser-sync en modo desarrollo ejecutamos en la terminal el siguiente comando:
```
$ npm i -D browser-sync
```
Otra manera de obtener browser-sync sin instalarlo es añadir en el archivo `package.json` en la parte de scripts lo siguiente:
```
"scripts": {
    "start": "npx browser-sync src -w"
  }
```
`NOTA:` `npx` es una herramienta de cli que nos permite ejecutar paquetes de npm de forma mucho más sencilla sin necesidad de instalarlos, `-w` lo usamos para poder hacer reload automático de nuestro proyecto

#### Algunos sitios de interes

  - [Browsersync](https://browsersync.io/)
  - [npx](https://www.npmjs.com/package/npx)
  - [JS Events](https://www.w3schools.com/js/js_events.asp)
  - [Events MDN](https://developer.mozilla.org/en-US/docs/Web/Events)
