const n1 = document.querySelector('#primer-numero')
const n2 = document.querySelector('#segundo-numero')
const sumButton = document.querySelector('#sum-button')
const difButton = document.querySelector('#dif-button')
const prodButton = document.querySelector('#prod-button')
const divButton = document.querySelector('#div-button')
const resultNode = document.querySelector('#result')

// sum opetion
sumButton.addEventListener('click', () => {

  // numeros trasnformados a enteros
  const n1Int = parseInt(n1.value)
  const n2Int = parseInt(n2.value)

  // operacion
  const result = n1Int + n2Int
  console.log(result)

  // creacion de nodo <span>(resultodao)<span>
  const textContent = document.createTextNode(result)
  const spanElement = document.createElement('span')
  spanElement.appendChild(textContent)

  // eliminar resultado previo
  if (resultNode.childNodes.length > 3) {
    resultNode.removeChild(resultNode.childNodes[3])
  }

  resultNode.appendChild(spanElement)
  return result
})

// diff operation
difButton.addEventListener('click', () => {

  // numeros trasnformados a enteros
  const n1Int = parseInt(n1.value)
  const n2Int = parseInt(n2.value)

  // operacion
  const result = n1Int - n2Int
  console.log(result)

  // creacion de nodo <span>(resultodao)<span>
  const textContent = document.createTextNode(result)
  const spanElement = document.createElement('span')
  spanElement.appendChild(textContent)

  // eliminar resultado previo
  if (resultNode.childNodes.length > 3) {
    resultNode.removeChild(resultNode.childNodes[3])
  }

  resultNode.appendChild(spanElement)
  return result
})

// prod operation
prodButton.addEventListener('click', () => {

  // numeros trasnformados a enteros
  const n1Int = parseInt(n1.value)
  const n2Int = parseInt(n2.value)

  // operacion
  const result = n1Int * n2Int
  console.log(result)

  // creacion de nodo <span>(resultodao)<span>
  const textContent = document.createTextNode(result)
  const spanElement = document.createElement('span')
  spanElement.appendChild(textContent)

  // eliminar resultado previo
  if (resultNode.childNodes.length > 3) {
    resultNode.removeChild(resultNode.childNodes[3])
  }

  resultNode.appendChild(spanElement)
  return result
})

// div operation
divButton.addEventListener('click', () => {

  // numeros trasnformados a enteros
  const n1Int = parseInt(n1.value)
  const n2Int = parseInt(n2.value)

  // operacion
  const result = n1Int / n2Int
  console.log(result)

  // creacion de nodo <span>(resultodao)<span>
  const textContent = document.createTextNode(result)
  const spanElement = document.createElement('span')
  spanElement.appendChild(textContent)

  // eliminar resultado previo
  if (resultNode.childNodes.length > 3) {
    resultNode.removeChild(resultNode.childNodes[3])
  }

  resultNode.appendChild(spanElement)
  return result
})