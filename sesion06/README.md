# Sesión 06
-------------------
#### Instalar dependencias y utilerías de webpack
Para instalar usaremos `npm i -D` de esta manera quedaran instaladas en las dependencias de desarrollo, en la terminal ejecutamos los siguientes comandos:
```
$ npm i -D webpack@4.44.2
```
```
$ npm i -D webpack-cli@3.3.12
```
```
$ npm i -D webpack-dev-server
```
```
$ npm i -D html-webpack-plugin
```
```
$ npm i -D clean-webpack-plugin
```
`Nota:` Con `@` podemos especificar la versión a instalar. De igual forma, podemos instalar todas las dependencias en un mismo comando separadas por un espacio entre ellas, como se mostrará en el siguiente ejemplo con las dependencias del entorno webpack.

#### Instalar dependencias del entorno webpack
```
npm i -D css-loader node-sass sass-loader style-loader
```
#### Algunos sitios de interes

  - [Webpack](https://webpack.js.org/)
  - [v4 Webpack](https://v4.webpack.js.org/)
  - [Webpack concepts](https://webpack.js.org/concepts/)
  - [Clean webpack plugin](https://www.npmjs.com/package/clean-webpack-plugin)
  - [HTML webpack plugin](https://www.npmjs.com/package/clean-webpack-plugin)
  - [webpack-dev-server](https://www.npmjs.com/package/webpack-dev-server)
  - [Using npm scripts](https://docs.npmjs.com/cli/v6/using-npm/scripts)
  - [The box model](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/The_box_model)
  - [Box sizing](https://css-tricks.com/box-sizing/)
  - [Box sizing MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/box-sizing)
