# Sesión 09
-------------------

#### Instalar dependencias y utilerías de webpack
```
$ npm i -D clean-webpack-plugin html-webpack-plugin webpack webpack-cli webpack-dev-server webpack-merge mini-css-extract-plugin css-minimizer-webpack-plugin
```

#### Instalar dependencias del entorno webpack
```
npm i -D css-loader node-sass sass-loader style-loader
```

#### Buscar comandos lanzados anteriormente en la consola
Tan solo debemos presionar `ctrl + r` y escribir un fragmento del comando a buscar, podemos ir saltando la busqueda presionando nuevamente `ctrl + r`

#### Borrar un directorio sin preguntar (forzado)
```
$ rm -rf node_modules
```
`NOTA:` Hay que tener precaución al utilizar el comando ya que eliminará el directorio y todo su contenido sin preguntar, no usarlo sino se sabe lo que se está eliminando, por error podríamos eliminar un directorio del sistema operativo

#### Algunos sitios de interes

  - [CSS Position MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/position)
  - [CSS position absolute](https://www.w3schools.com/css/tryit.asp?filename=trycss_position_absolute)
  - [Object fit](https://developer.mozilla.org/en-US/docs/Web/CSS/object-fit)